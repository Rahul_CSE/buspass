var express = require('express');
var router = express.Router();
const bcrypt = require('bcrypt');
const session = require('express-session');
const saltRounds = 10;
var url = "mongodb://127.0.0.1:27017/"
var MongoClient = require('mongodb').MongoClient;

const app = express();


app.use(session({
  secret: 'keyboard cat',saveUninitialized: true,resave: true}
))


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/about', function(req, res, next) {
  res.render('about');
});

router.get('/contact', function(req, res, next) {
  res.render('contact');
});

router.get('/login', function(req, res, next) {
  res.render('login',{errno:0});
});

router.get('/reset', function(req, res, next) {
  res.render('reset',{errno:0});
});

router.get('/logout',function(req,res,next){
  req.session.destroy();
  res.redirect('/')
});

router.post('/con',function(req,res,next){
  res.render('contact');
})

router.get('/b1',function(req,res,next){
  res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');

  if(!req.session.user)
  {
    res.redirect('/')
  }
  var aYearFromNow = new Date();
  aYearFromNow.setFullYear(aYearFromNow.getFullYear() + 1);
  var toDate = aYearFromNow.toISOString().slice(0, 10);
  ob={email:req.session.user.email,source:'Udupi',destination:'Mangalore',price:'300'}
  MongoClient.connect(url, { useUnifiedTopology: true, useNewUrlParser: true }, function (er, db) {
    dbo = db.db("buspass");
      // Store hash in your password DB.
      dbo.collection("book").findOne(ob,function(err,re)
      {
        try{
        if(re){
        
          res.render('book',{errno:3})
        }
        else
        {
          ob={email:req.session.user.email,source:'Udupi',destination:'Mangalore',price:'300',exp:toDate}
          dbo.collection("book").insertOne(ob, function (err, re) {
            if(err)
            {  
            res.render('book',{errno:2})
            }  
          })
        
        }
      }
        finally
        {
          res.render('book',{errno:1})
      }
      })
        
    });
  });

router.get('/b2',function(req,res,next){
  res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');

  if(!req.session.user)
  {
    res.redirect('/')
  }
  var aYearFromNow = new Date();
  aYearFromNow.setFullYear(aYearFromNow.getFullYear() + 1);
  var toDate = aYearFromNow.toISOString().slice(0, 10);
  ob={email:req.session.user.email,source:'Kundapur',destination:'udupi',price:'500'}
  MongoClient.connect(url, { useUnifiedTopology: true, useNewUrlParser: true }, function (er, db) {
    dbo = db.db("buspass");
      // Store hash in your password DB.
      dbo.collection("book").findOne(ob,function(err,re)
      {
        try{
        if(re){
        
          res.render('book',{errno:3})
        }
        else
        {
          ob={email:req.session.user.email,source:'Kundapur',destination:'udupi',price:'500',exp:toDate}
          dbo.collection("book").insertOne(ob, function (err, re) {
            if(err)
            {  
            res.render('book',{errno:2})
            }  
          })
        
        }
      }
        finally
        {
          res.render('book',{errno:1})
      }
      })
        
    });
  });

router.get('/b3',function(req,res,next){
  res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');

  if(!req.session.user)
  {
    res.redirect('/')
  }
  var aYearFromNow = new Date();
  aYearFromNow.setFullYear(aYearFromNow.getFullYear() + 1);
  var toDate = aYearFromNow.toISOString().slice(0, 10);
  ob={email:req.session.user.email,source:'Kundapur',destination:'Mangalore',price:'700'}
  MongoClient.connect(url, { useUnifiedTopology: true, useNewUrlParser: true }, function (er, db) {
    dbo = db.db("buspass");
      // Store hash in your password DB.
      dbo.collection("book").findOne(ob,function(err,re)
      {
        try{
        if(re){
        
          res.render('book',{errno:3})
        }
        else
        {
          ob={email:req.session.user.email,source:'Kundapur',destination:'Mangalore',price:'700',exp:toDate}
          dbo.collection("book").insertOne(ob, function (err, re) {
            if(err)
            {  
            res.render('book',{errno:2})
            }  
          })
        
        }
      }
        finally
        {
          res.render('book',{errno:1})
      }
      })
        
    });
  
});

router.get('/b4',function(req,res,next){
  res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');

  if(!req.session.user)
  {
    res.redirect('/')
  }
  var aYearFromNow = new Date();
  aYearFromNow.setFullYear(aYearFromNow.getFullYear() + 1);
  var toDate = aYearFromNow.toISOString().slice(0, 10);
  ob={email:req.session.user.email,source:'Udupi',destination:'Karkala',price:'400'}
  MongoClient.connect(url, { useUnifiedTopology: true, useNewUrlParser: true }, function (er, db) {
    dbo = db.db("buspass");
      // Store hash in your password DB.
      dbo.collection("book").findOne(ob,function(err,re)
      {
        try{
        if(re){
        
          res.render('book',{errno:3})
        }
        else
        {
          ob={email:req.session.user.email,source:'Udupi',destination:'Karkala',price:'400',exp:toDate}
          dbo.collection("book").insertOne(ob, function (err, re) {
            if(err)
            {  
            res.render('book',{errno:2})
            }  
          })
        
        }
      }
        finally
        {
          res.render('book',{errno:1})
      }
      })
        
    });
  });

router.get('/b5',function(req,res,next){
  res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');

  if(!req.session.user)
  {
    res.redirect('/')
  }
  var aYearFromNow = new Date();
  aYearFromNow.setFullYear(aYearFromNow.getFullYear() + 1);
  var toDate = aYearFromNow.toISOString().slice(0, 10);
  ob={email:req.session.user.email,source:'Kundapur',destination:'Mangalore',price:'600'}
  MongoClient.connect(url, { useUnifiedTopology: true, useNewUrlParser: true }, function (er, db) {
    dbo = db.db("buspass");
      // Store hash in your password DB.
      dbo.collection("book").findOne(ob,function(err,re)
      {
        try{
        if(re){
        
          res.render('book',{errno:3})
        }
        else
        {
          ob={email:req.session.user.email,source:'Kundapur',destination:'Mangalore',price:'600',exp:toDate}
          dbo.collection("book").insertOne(ob, function (err, re) {
            if(err)
            {  
            res.render('book',{errno:2})
            }  
          })
        
        }
      }
        finally
        {
          res.render('book',{errno:1})
      }
      })
        
    });
  });

router.get('/b6',function(req,res,next){
  res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');

  if(!req.session.user)
  {
    res.redirect('/')
  }
  var aYearFromNow = new Date();
  aYearFromNow.setFullYear(aYearFromNow.getFullYear() + 1);
  var toDate = aYearFromNow.toISOString().slice(0, 10);
  ob={email:req.session.user.email,source:'Mangalore',destination:'Karkala',price:'400'}
  MongoClient.connect(url, { useUnifiedTopology: true, useNewUrlParser: true }, function (er, db) {
    dbo = db.db("buspass");
      // Store hash in your password DB.
      dbo.collection("book").findOne(ob,function(err,re)
      {
        try{
        if(re){
        
          res.render('book',{errno:3})
        }
        else
        {
          ob={email:req.session.user.email,source:'Mangalore',destination:'Karkala',price:'400',exp:toDate}
          dbo.collection("book").insertOne(ob, function (err, re) {
            if(err)
            {  
            res.render('book',{errno:2})
            }  
          })
        
        }
      }
        finally
        {
          res.render('book',{errno:1})
      }
      })
        
    });
  });

router.get('/profile', function(req, res, next) {
  res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');

  if(!req.session.user)
  {
    res.redirect('/')
  }
  res.render('profile',{data:req.session.user});
});

router.get('/book', function(req, res, next) {
  res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');

  if(!req.session.user)
  {
    res.redirect('/')
  }
  res.render('book',{errno:0});
});

router.get('/home', function(req, res, next) {
  res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
  k=[]
  if(!req.session.user)
  {
    res.redirect('/')
  }
  else
  {
    (async () => {
    let client = await MongoClient.connect(url,
      { useNewUrlParser: true });
    
      let db = client.db('buspass');
      try{
      const res2 = await db.collection("book").find({
        "email": req.session.user.email
      }).toArray();
      k.push(res2)
    }
    finally{
      
      if(k[0] && k[0].length)
      {
        res.render('home',{data:k[0]})
      }
      else{
        res.render('home',{data:[{source:'NA',destination:'NA',price:'NA',exp:'NA'}]})
      }
    }
    }
    )()
        .catch(err => console.error(err));
  }
});

router.post('/signup',function(req,res,next){
  MongoClient.connect(url, { useUnifiedTopology: true, useNewUrlParser: true }, function (er, db) {
    dbo = db.db("buspass");
    bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
      // Store hash in your password DB.
  
    ob = { name:req.body.name,email:req.body.email,phno:req.body.phno,schoolid:req.body.schoolid,schoolname:req.body.schoolname,password:hash,sess:'n' }
    dbo.collection("user").insertOne(ob, function (err, re) {
      if (err) {
        res.render('login',{errno:2})
      }
    })
  });
});
res.render('login',{errno:1})
});

router.post('/signin',function(req,res,next){
  MongoClient.connect(url, { useUnifiedTopology: true, useNewUrlParser: true }, function (er, db) {
    dbo = db.db("buspass");
    ob = { email:req.body.email}
    dbo.collection("user").findOne(ob)
    .then(user =>{
      if(!user)
      {
        res.render('login',{errno:2})
      }
      else
      {
        bcrypt.compare(req.body.password,user.password,(err,isMatch)=>{
          if(err) throw err;
          if(isMatch)
          {
            req.session.user = user;
            res.redirect('/home');
          }
          else
          {
            res.render('login',{errno:2})
          }
        })
      }
    })
    .catch(err =>{

      res.render('login',{errno:2})
    })
});
});

router.post('/reset',function(req,res,next){
  MongoClient.connect(url, { useUnifiedTopology: true, useNewUrlParser: true }, function (er, db) {
    dbo = db.db("buspass");
    ob = { email:req.body.email}
    dbo.collection("user").findOne(ob)
    .then(user =>{
      if(!user)
      {
        res.render('reset',{errno:3})
      }
      else
      {
        bcrypt.genSalt(10, (er, salt) => {
          bcrypt.hash(req.body.password, salt, (err, hash) => {
            if (err) throw err;
            dbo.collection("user").updateOne({email:req.body.email},{$set:{password:hash}},function(er,re){
              if(er) {
                res.render('reset',{errno:2})
              }
              else
              {
                res.render('reset',{errno:1})
              }
            })
          })
        })
      }
    })
    .catch(err =>{

      res.render('reset',{errno:2})
    })
});
});


module.exports = router;
